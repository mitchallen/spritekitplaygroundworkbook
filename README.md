Swift SpriteKit: Playground Animation Workbook
==============================================

Code examples
-------------------------------------------------

## About

The code examples in this repo are for the [__Swift SpriteKit: Playground Animation Workbook__ by Mitch Allen](http://www.amazon.com/dp/B01A7WSGBA/). You can get the Kindle edition here: [http://www.amazon.com/dp/B01A7WSGBA/](http://www.amazon.com/dp/B01A7WSGBA/).

## Requirements

The examples require Xcode installed on a Mac. Xcode is available for free from the Mac App Store.

* * *

## Usage

Open a Playground (.playground) file in Xcode.

Most examples should run automatically.

In order to see the results, the Xcode Assistant Editor window must be shown. 

If it is not shown, select from the Xcode main menu:  __View / Assistant Editor / Show Assistant Editor__

* * *

## Web Site

* [http://mitchallen.com](http://mitchallen.com)

* * *

## Repos

* [bitbucket.org/mitchallen/spritekitplaygroundworkbook.git](https://bitbucket.org/mitchallen/spritekitplaygroundworkbook.git)

* * *

## Version History

#### Version 1.0 release notes

* Added example code for Xcode 7.2