// 1. Setup Scene and View

import UIKit
import SpriteKit
import XCPlayground

let sceneWidth:CGFloat = 1024
let sceneHeight:CGFloat = 768

let sceneSize = CGSizeMake(sceneWidth, sceneHeight)

let view:SKView = SKView(frame:
    CGRectMake(0, 0, sceneWidth, sceneHeight))

XCPlaygroundPage.currentPage.liveView = view

let scene:SKScene = SKScene(size: sceneSize)
scene.backgroundColor = SKColor.blackColor()
scene.scaleMode = SKSceneScaleMode.AspectFit
view.presentScene(scene)

// 2. Define Sprite(s)

let sprite = SKSpriteNode()
sprite.size = CGSizeMake(300,100)
sprite.color = SKColor.greenColor()

sprite.position = CGPointMake(
    CGRectGetMidX(view.frame),
    CGRectGetMidY(view.frame))

// 3. Define Action(s)

let rotateAction = SKAction.rotateByAngle(
    CGFloat(M_PI), duration:1)

let action = SKAction.repeatActionForever(rotateAction)

// 4. Run Action(s)

sprite.runAction(action)

// 5. Add Sprite(s) to the Scene

scene.addChild(sprite)