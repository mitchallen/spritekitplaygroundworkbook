// 1. Setup Scene and View

import UIKit
import SpriteKit
import XCPlayground

let sceneWidth:CGFloat = 1024
let sceneHeight:CGFloat = 768

let sceneSize = CGSizeMake(sceneWidth, sceneHeight)

let view:SKView = SKView(frame:
    CGRectMake(0, 0, sceneWidth, sceneHeight))

XCPlaygroundPage.currentPage.liveView = view

let scene:SKScene = SKScene(size: sceneSize)
scene.backgroundColor = SKColor.blackColor()
scene.scaleMode = SKSceneScaleMode.AspectFit
view.presentScene(scene)

// 2. Define Sprite(s)

let texture = SKTexture(imageNamed: "butterfly-red")

let sprite = SKSpriteNode(texture: texture)

sprite.xScale = 0.5
sprite.yScale = 0.5

sprite.position = CGPointMake(
    CGRectGetMidX(view.frame),
    CGRectGetMidY(view.frame))

// 3. Define Action(s)

let point = CGPoint(
    x:CGRectGetMidX(view.frame) * 0.5,
    y:CGRectGetMidY(view.frame) * 0.5)

let degrees:CGFloat = 135

let angle:CGFloat = degrees * CGFloat(M_PI) / 180

let rotateAction = SKAction.rotateByAngle(angle, duration:5)

let moveAction = SKAction.moveTo(point, duration: 5)

let action = SKAction.sequence([rotateAction,moveAction])

// 4. Run Action(s)

sprite.runAction(action)

// 5. Add Sprite(s) to the Scene

scene.addChild(sprite)
