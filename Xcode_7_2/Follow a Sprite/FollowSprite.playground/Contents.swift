// 1. Setup Scene and View

import UIKit
import SpriteKit
import XCPlayground

let sceneWidth:CGFloat = 1024
let sceneHeight:CGFloat = 768

let sceneSize = CGSizeMake(sceneWidth, sceneHeight)

let view:SKView = SKView(frame:
    CGRectMake(0, 0, sceneWidth, sceneHeight))

XCPlaygroundPage.currentPage.liveView = view

let scene:SKScene = SKScene(size: sceneSize)
scene.backgroundColor = SKColor.blackColor()
scene.scaleMode = SKSceneScaleMode.AspectFit
view.presentScene(scene)

// 2. Define Sprite(s)

let texture = SKTexture(imageNamed: "butterfly-red")

let sprite = SKSpriteNode(texture: texture)

// Set the original sprites scale to 0.25

sprite.xScale = 0.25
sprite.yScale = 0.25

// Start the original sprite higher than usual

sprite.position = CGPointMake(
    CGRectGetMidX(view.frame) - 200,
    CGRectGetMidY(view.frame) + 200)

// Define a second sprite

let texture2 = SKTexture(imageNamed: "butterfly-yellow")

let sprite2 = SKSpriteNode(texture: texture2)
sprite2.xScale = 0.25
sprite2.yScale = 0.25
sprite2.position = CGPointMake(
    CGRectGetMidX(view.frame) - 400,
    CGRectGetMidY(view.frame) - 400)

// 3. Define Action(s)

let rotationRange
= SKRange(
    lowerLimit: CGFloat(M_PI_2*7),
    upperLimit: CGFloat(M_PI_2*7))

let rotationConstraint
= SKConstraint.orientToNode(
    sprite, offset: rotationRange)

let distanceRange = SKRange(
    lowerLimit: 100.0, upperLimit: 150.0)

let distanceConstraint = SKConstraint.distance(
    distanceRange, toNode: sprite)

sprite2.constraints = [
    rotationConstraint, distanceConstraint]

// Define action for first (red) sprite

let turn90 = SKAction.rotateByAngle(CGFloat(-M_PI_2), duration:0.1)
let turn180 = SKAction.rotateByAngle(CGFloat(-M_PI), duration:0.1)
let move1 = SKAction.moveByX(400, y:0, duration: 2)
let move2 = SKAction.moveByX(0, y:-400, duration: 2)
let move3 = SKAction.moveByX(-400, y:0, duration: 2)
let move4 = SKAction.moveByX(0, y:400, duration: 2)
let move = SKAction.sequence(
    [turn90,move1,turn90,move2,
        turn90,move3,turn90,move4])
let backup = move.reversedAction()
let sequence = SKAction.sequence([move,turn180,backup,turn180])
let action = SKAction.repeatActionForever(sequence)

// 4. Run Action(s)

sprite.runAction(action)

// 5. Add Sprite(s) to the Scene

scene.addChild(sprite)
scene.addChild(sprite2)
