// 1. Setup Scene and View

import UIKit
import SpriteKit
import XCPlayground

let sceneWidth:CGFloat = 1024
let sceneHeight:CGFloat = 768

let sceneSize = CGSizeMake(sceneWidth, sceneHeight)

let view:SKView = SKView(frame:
    CGRectMake(0, 0, sceneWidth, sceneHeight))

XCPlaygroundPage.currentPage.liveView = view

let scene:SKScene = SKScene(size: sceneSize)
scene.backgroundColor = SKColor.blackColor()
scene.scaleMode = SKSceneScaleMode.AspectFit
view.presentScene(scene)

// 2. Define Sprite(s)

let texture = SKTexture(imageNamed: "butterfly-red")

let sprite = SKSpriteNode(texture: texture)

sprite.xScale = 0.5
sprite.yScale = 0.5

// Change the first sprites default location

sprite.position = CGPointMake(
    CGRectGetMidX(view.frame) - 300,
    CGRectGetMidY(view.frame) + 200)

// Define a second sprite:

let texture2 = SKTexture(imageNamed: "arrow-white")

let sprite2 = SKSpriteNode(texture: texture2)

sprite2.xScale = 0.5
sprite2.yScale = 0.5

sprite2.position = CGPointMake(
    CGRectGetMidX(view.frame),
    CGRectGetMidY(view.frame) - 200)

// 3. Define Action(s)

let rotationRange
= SKRange(
    lowerLimit: CGFloat(M_PI_2*7),
    upperLimit: CGFloat(M_PI_2*7))

let rotationConstraint
= SKConstraint.orientToNode(sprite, offset: rotationRange)

sprite2.constraints = [rotationConstraint]

let moveAction = SKAction.moveByX(400, y:0, duration: 2)
let reverseAction = moveAction.reversedAction()
let sequence = SKAction.sequence([moveAction,reverseAction])

let action = SKAction.repeatActionForever(sequence)

// 4. Run Action(s)

sprite.runAction(action)

// 5. Add Sprite(s) to the Scene

scene.addChild(sprite)
scene.addChild(sprite2)
