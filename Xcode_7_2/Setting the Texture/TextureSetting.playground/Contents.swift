// 1. Setup Scene and View

import UIKit
import SpriteKit
import XCPlayground

let sceneWidth:CGFloat = 1024
let sceneHeight:CGFloat = 768

let sceneSize = CGSizeMake(sceneWidth, sceneHeight)

let view:SKView = SKView(frame:
    CGRectMake(0, 0, sceneWidth, sceneHeight))

XCPlaygroundPage.currentPage.liveView = view

let scene:SKScene = SKScene(size: sceneSize)
scene.backgroundColor = SKColor.blackColor()
scene.scaleMode = SKSceneScaleMode.AspectFit
view.presentScene(scene)

// 2. Define Sprite(s)

let texture = SKTexture(imageNamed: "butterfly-red")

let sprite = SKSpriteNode(texture: texture)

sprite.xScale = 0.5
sprite.yScale = 0.5

sprite.position = CGPointMake(
    CGRectGetMidX(view.frame),
    CGRectGetMidY(view.frame))

// 3. Define Action(s)

let newTexture = SKTexture(imageNamed: "butterfly-select")

let oldAction = SKAction.setTexture(texture)

let newAction = SKAction.setTexture(newTexture)

let wait = SKAction.waitForDuration(5)

let sequence = SKAction.sequence(
    [oldAction,wait,newAction,wait])

let action = SKAction.repeatActionForever(sequence)

// 4. Run Action(s)

sprite.runAction(action)

// 5. Add Sprite(s) to the Scene

scene.addChild(sprite)
